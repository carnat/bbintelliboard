<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'DashboardController@index');
Route::get('/get-for-chart', 'DashboardController@getForChart');
Route::any('/caliper', 'DashboardController@caliper');
Route::get('/test', 'DashboardController@test');