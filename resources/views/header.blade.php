<header class="">
    <div class="logo">
        <a href="https://app.intelliboard.net/">
            <img src="/img/logo.png" alt="logo" style="border-bottom: 1px solid #e9ebf0;
             border-right: 1px solid #e9ebf0; margin-bottom: 2px;">
            {{--<span class="icon"><i class="intelliboard"></i></span>--}}
            <span class="text">IntelliBoard</span>
        </a>
    </div>
    <div class="header-content clearfix">
        <div class="left">
            <div class="dropdown serversmenu">
                <a class="dropdown-toggle clearfix" id="serversMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                    <strong>Demo</strong>
                    <span><i class="ion-chevron-down"></i></span>
                </a>

                <div class="dropdown-menu w300 alignToLeft withHeader" aria-labelledby="serversMenu">
                    <div class="dropdown-header clearfix">
                        <span>Connections:</span>
                        <a href="https://app.intelliboard.net/settings/servers"><i class="ion-ios-gear-outline"></i></a>

                    </div>
                    <div class="dropdown-body">
                        <ul class="servers-list jsactive">
                            <div>
                                <li class="clearfix active" id="server_menu_item_2678">
                                    <a href="https://app.intelliboard.net/servers/view/2678">
                                        <strong>Demo</strong>
                                        <p>https://bblearn.intelliboard.nett</p>
                                    </a><span><i class="server-checkbox   selected  ion-android-checkbox-outline" data-id="2678"></i>
                  </span></li>
                            </div>
                            <li role="separator" class="divider"></li>
                            <li class="clearfix"><a class="addnew" href="https://app.intelliboard.net/settings/server/add"><i class="ion-plus"></i> Add a new connection</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="right clearfix">
            <div class="header-alert">
                <span>355 days remaining</span>
                <a href="https://app.intelliboard.net/subscription/billing">UPGRADE NOW <i class="ion-ios-heart-outline"></i></a>
            </div>

            <div class="header-search">
                <label id="global-search" for="global-search-input">
                    <div class="lisa-icon"></div>
                    <span>Ask LISA</span>
                </label>
            </div>

            <div class="header-user">
                <div class="dropdown">
                    <a class="dropdown-toggle" type="button" id="userMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                        <i class="ion-person"></i>
                        <i class="ion-chevron-down"></i>
                    </a>

                    <div class="dropdown-menu w350 alignToRight dropdown-menu-right" aria-labelledby="userMenu">
                        <div class="dropdown-body user-settings">
                            <div class="user-header clearfix">
                                <div class="user-avatar">
                                    <i class="ion-person"></i>
                                </div>
                                <div class="user-info">
                                    <strong>Anatoliy Kochnev</strong>
                                    <p>anatoliy@intelliboard.net</p>
                                </div>
                            </div>



                            <div class="user-stats">
                                <div class="stat">
                                    <p>IntelliBoard Users: <strong>0</strong>/1 (0%)</p>
                                    <div class="chart"><span class="" style="width:0%"></span></div>
                                </div>

                                <div class="stat">
                                    <p>Connections: <strong>1</strong>/1 (100%)</p>
                                    <div class="chart"><span class="danger" style="width:100%"></span></div>
                                </div>

                                <div class="stat">
                                    <p>Moodle Learners: <strong>30</strong>/500 (6%)</p>
                                    <div class="chart"><span class="" style="width:6%"></span></div>
                                </div>
                            </div>
                            <ul class="user-menu">
                                <li><a href="https://app.intelliboard.net/profile">Account settings</a></li>
                                <li><a href="https://app.intelliboard.net/subscription/billing">Billing Details</a></li>
                                <li><a href="https://app.intelliboard.net/signout">Log out</a></li>
                            </ul>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</header>