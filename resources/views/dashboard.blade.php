@extends('template')

@section('content')
    <div class='row'>
        <div class="panel panel-summary grid-item clearfix" id="monitors">
            <div class="panel-left">
                <div class="panelHeader clearfix">
                    <strong>IntelliBoard Home <i class="ion-help-circled"></i></strong>
                    <span>Last Update at <?php echo date("h:i"); ?></span>
                </div>
                <div class="panelBody">
                    <div class="monitor_body_28">
                        <ul class="panel-totals clearfix">
                            <li>
                                <strong id="total-users">{{count($users)}}</strong>
                                <span class="desc">Users</span>
                            </li>
                            <li>
                                <strong id="total-courses">{{count($courses)}}</strong>
                                <span class="desc">Courses</span>
                            </li>
                            <li>
                                <strong id="total-modules">{{count($courseTools)}}</strong>
                                <span class="desc">Modules</span>
                            </li>
                        </ul>
                    </div>
                    <div class="monitor_body_21">
                        <div id="wrapper" style="position: relative; height: 300px">
                            <canvas id="canvas"></canvas>
                        </div>
                    </div>
                </div>
                <div class="panelFooter clearfix">

                </div>
            </div>
            <div class="panel-right">
                <div class="panelHeader clearfix">
                    <strong>When do your learners visit <i class="ion-help-circled"></i></strong>
                </div>
                <div class="panelBody">
                    <div class="monitor_body_22">
                        <div class="grid-chart">
                            <table>
                                <?php for ($i = 0; $i < 24; $i++): ?>
                                <tr>
                                    <?php for ($s = 0; $s < 7; $s++): ?>
                                    <?php
                                    $visits = 0;
                                    foreach($sessionActivity as $activity){
                                        $date = new DateTime($activity->login_time);
                                        if($date->format('w') == $s AND $date->format('h') == $i) {
                                            $visits++;
                                        }
                                    }
                                    $opacity = $visits / count($sessionActivity);
                                    ?>
                                    <td class="timeofyearkey{{number_format($opacity, 1)}} ">
                                        <div class="timeofday" title="<?php echo $weeks[$s] . ' ' . date("ga", strtotime("$i:00")) . ": $visits  page visits"; ?>"><span class="active" style="opacity:<?php echo $opacity; ?>"></span></div>
                                    </td>
                                    <?php endfor; ?>

                                    <?php if (($i % 2) == 0): ?>
                                    <td class="empty-month2">
                                        <div class="timeofdaylabel"><?php echo date("ga", strtotime("$i:00")); ?></div>
                                    </td>
                                    <?php else: ?>
                                    <td class="empty-month2"></td>
                                    <?php endif; ?>
                                </tr>
                                <?php endfor; ?>
                                <tr>
                                    <?php foreach ($weeks as $day): ?>
                                    <td>
                                        <div class="timeofweek"><?php echo $day; ?></div>
                                    </td>
                                    <?php endforeach; ?>
                                    <td class="empty-month2"></td>
                                </tr>
                            </table>
                            <ul class="grid-chart-labels clearfix">
                                <li><p class="opacity20 timeofyearkey0.2"></p><span>&nbsp;0%</span></li>
                                <li><p class="opacity50 timeofyearkey0.5"></p><span>25</span></li>
                                <li><p class="opacity80 timeofyearkey0.8"></p><span>50</span></li>
                                <li><p></p><span>75</span></li>
                                <li class="last"><span>100</span></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="panelFooter clearfix">

                </div>
            </div>
        </div>



        <div class="panel-live grid-item">
            <h3>Online right now <i class="ion-contrast" id="livecontrast"></i></h3>
            <div class="monitor_body_13">
                <div class="recentinfo">
                    <ul class="totals clearfix">
                        <li>
                            <strong id="online-learners">{{$currentLearners}}</strong>
                            <span>learners</span>
                        </li>
                        <li>
                            <strong id="online-teachers">{{$currentTeachers}}</strong>
                            <span>teachers</span>
                        </li>
                        <li>
                            <strong id="online-users">{{$currentOnline}}</strong>
                            <span>all users</span>
                        </li>
                    </ul>
                    <div class="visual">
                        <div class="visual-head">Page views per hour</div>
                        <table class="visual-chart">
                            <tr>
                                <?php for($i = 0; $i< 24; $i++): ?>
                                <?php
                                $height = rand(0, 100);
                                ?>
                                <td class="timeofyearkey<?php echo '0'; ?>">
                                    <div class="visual-col <?php echo $i % 2 == 0 ? 'active' : ''; ?>"
                                         style="height:<?php echo ($height / 100) * 70; ?>px;"
                                         title="<?php echo date("ga", strtotime("$i:00")); ?>: <?php echo 0; ?> visits"></div>
                                </td>
                                <?php endfor; ?>
                            </tr>
                        </table>
                    </div>


                    <table class="summary">
                        <thead>
                        <tr>
                            <th>Event yesterday</th>
                            <th style="text-align: right">Value</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>Enrollments</td>
                            <td align="right" id="live-enrolments">{{$enrollmentsPerDay[$now->format('Y-m-d')] ?? 0}}</td>
                        </tr>
                        <tr>
                            <td>Activity completions</td>
                            <td align="right" id="live-completed_activities">{{$completionPerDay[$now->format('Y-m-d')] ?? 0}}</td>
                        </tr>
                        <tr>
                            <td>New registrations</td>
                            <td align="right" id="live-registrations">{{$newUsers}}</td>
                        </tr>
                        <tr>
                            <td>Visits</td>
                            <td align="right" id="live-visits">{{$views}}</td>
                        </tr>
                        <tr>
                            <td>Time spent</td>
                            <td align="right" id="live-timespend">{{(int)($timeSpent / 60)}}h {{$timeSpent % 60}}min</td>
                        </tr>
                        <tr>
                            <td>Sessions</td>
                            <td align="right" id="live-sessions">{{$sessionsPerDay[$now->format('Y-m-d')] ?? 0}}</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="panel panel-report grid-item clearfix">
            <div class="monitor_body_4">
                <div class="monitor-table">
                    <div class="monitor-table-header clearfix">
                        <strong>Users Overview <i class="ion-help-circled"></i></strong>
                    </div>
                    <div class="monitor-table-body">
                        <div id="report-43_wrapper" class="dataTables_wrapper no-footer"><div class="clear"></div><div id="report-43_processing" class="dataTables_processing" style="display: none;">Processing...</div><table id="report-43" class="ajaxtable table-data fixed" role="grid"><thead><tr role="row"><th width="0" align="left" class="0 sorting_asc" tabindex="0" aria-controls="report-43" rowspan="1" colspan="1" aria-label="Learner: activate to sort column descending" aria-sort="ascending">Learner</th><th width="0" align="center" class="0 sorting" tabindex="0" aria-controls="report-43" rowspan="1" colspan="1" aria-label="Progress: activate to sort column ascending">Progress</th><th width="0" align="center" class="0 sorting" tabindex="0" aria-controls="report-43" rowspan="1" colspan="1" aria-label="Score: activate to sort column ascending">Score</th><th width="0" align="center" class="0 sorting" tabindex="0" aria-controls="report-43" rowspan="1" colspan="1" aria-label="Visits: activate to sort column ascending">Visits</th><th width="0" align="center" class="0 sorting" tabindex="0" aria-controls="report-43" rowspan="1" colspan="1" aria-label="Time Spent: activate to sort column ascending">Time Spent</th><th width="0" align="center" class="0 sorting" tabindex="0" aria-controls="report-43" rowspan="1" colspan="1" aria-label="Registered: activate to sort column ascending">Registered</th></tr></thead>
                                <tbody>
                                @foreach ($users as $user)
                                    @if ($user->institution_roles_pk1 == 1)
                                        <tr role="row" class="even">
                                            <td class="sorting_1" align="left">
                                                <a href="/">{{$user->firstname .' '.$user->lastname}}</a>
                                            </td>
                                            <td align="center">
                                                <div class="progress s" title="Enrolled: {{$user->enrollments ?? 0}} Completed: #">
                                                    <div class="progress-bar progress-bar-success" role="progressbar"
                                                         aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"
                                                         style="width: 0%">
                                                        <span class="sr-only">0% Progress</span>
                                                    </div>
                                                </div>
                                            </td>
                                            <td align="center">
                                                <div class="intelliboard-tooltip" title="{{$user->firstname .' '.$user->lastname}} grade: {{$user->sumGrade}}, Average grade: {{$avgGrade}}">
                                                    <span class="{{$user->sumGrade > $avgGrade ?
                                                     'up ion-arrow-graph-up-left':'down ion-arrow-graph-down-left'}}"> {{$user->sumGrade}}</span>
                                                </div>
                                            </td>
                                            <td align="center">
                                                <?php $pages = mt_rand(0, 400); ?>
                                                <div class="intelliboard-tooltip" title="{{$user->firstname .' '.$user->lastname}} visits: {{$pages}}, Average visits: {{$avgPages}}">
                                                    <span class="{{$pages > $avgPages ?
                                                     'up ion-arrow-graph-up-left' : 'down ion-arrow-graph-down-left'}}"> {{$pages}}</span>
                                                </div>
                                            </td>
                                            <td align="center">
                                                <div class="intelliboard-tooltip"
                                                     title="{{$user->firstname .' '.$user->lastname}} time: {{isset($user->timeSpent) ? (int)($user->timeSpent / 60).':'.$user->timeSpent % 60 : '00:00'}}:00, Average time: {{(int)($avgTimeSpent / 60)}}:{{$avgTimeSpent % 60}}:00">
                                                    <span class="{{$user->timeSpent > $avgTimeSpent ?
                                                     'up ion-arrow-graph-up-left' : 'down ion-arrow-graph-down-left'}}">{{isset($user->timeSpent) ? (int)($user->timeSpent / 60).':'.$user->timeSpent % 60 : '00:00'}}:00</span>
                                                </div>
                                            </td>
                                            <td align="center">{{(new DateTime($user->dtcreated))->format('m/d/Y')}}</td>
                                        </tr>
                                    @endif
                                @endforeach
                                </tbody>
                            </table>
                            <div class="dataTables_paginate paging_simple" id="report-43_paginate"></div>
                        </div>
                    </div>
                    <div class="monitor-table-footer clearfix"></div>
                </div>
            </div>
        </div>

    </div><!-- /.row -->
@endsection