<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Api\v1\InstitutionRole;

/**
 * Class Cache
 * @package App\Console\Commands
 */
class Cache extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cache:warmup';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Warmup cache for BB API';

    /**
     * Import handler
     */
    public function handle()
    {

    }
}
