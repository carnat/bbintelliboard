<?php

namespace App\Api\v1;


class Announcement extends ActiveResource
{
    /**
     * @inheritdoc
     */
    protected static function endpointName()
    {
        return 'announcements';
    }
}
