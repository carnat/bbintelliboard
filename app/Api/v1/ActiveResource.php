<?php

namespace App\Api\v1;

use Illuminate\Support\Facades\Cache;

class ActiveResource extends Endpoint
{
    const DEFAULT_PAGINATION_LIMIT = 100;

    protected $data;

    /**
     * Disable the creation of an object in the usual way
     */
    private function __construct(){}

    /**
     * Returns the fully qualified name of this class.
     * @return string the fully qualified name of this class.
     */
    public static function className() : string
    {
        return get_called_class();
    }

    /**
     * Returns all entities
     * @param array $params Search params (offset, limit, other criteria)
     * @param string $endpoint Custom endpoint URL
     * @param bool $withCache Enable or disable cache
     * @return array
     */
    public static function find(array $params = [], string $endpoint = null, bool $withCache = true) : array
    {
        //TODO in future here must be client ID from database
        $clientId = auth()->user() ? auth()->user()->id : 0;

        $cacheName = $clientId . get_called_class() . $endpoint . serialize($params);
        $entities = Cache::store('redis')->get($cacheName);

        if (!$withCache || !$entities) {
            $data = static::get($params, $endpoint);
            $count = isset($data->content->results) ? count($data->content->results) : 0;

            //API returns maximum 100 records. Try to get more than 100 records
            if (
                !isset($params['offset']) &&
                $count == static::DEFAULT_PAGINATION_LIMIT &&
                isset($data->content->paging)
            ) {
                $offset = static::findOffset($data->content->paging->nextPage);
                while ($count == static::DEFAULT_PAGINATION_LIMIT && $offset) {
                    $params['offset'] = $offset;
                    $batch = static::get($params, $endpoint);
                    $content = $batch->content;
                    $count = count($content->results);
                    $data->content->results = array_merge($data->content->results, $content->results);
                    $offset = isset($content->paging) ? static::findOffset($content->paging->nextPage) : null;
                }
            }

            $entities = static::populateMultiple($data);
            Cache::store('redis')->put([$cacheName => $entities], now()->addHour());
        }

        return $entities;
    }

    /**
     * Returns one entity by ID
     * @param string $id Entity ID
     * @param string $endpoint Custom endpoint
     * @param bool $withCache Enable or disable cache
     * @return null|ActiveResource
     */
    public static function findOne(string $id, string $endpoint = null, bool $withCache = true) : ?ActiveResource
    {
        //TODO in future here must be client ID from database
        $clientId = auth()->user() ? auth()->user()->id : 0;

        $cacheName = $clientId . $id . get_called_class() . $endpoint;
        $entity = Cache::store('redis')->get($cacheName);

        if (!$withCache || !$entity) {
            $data = static::get(['id' => $id], $endpoint);
            $entity = static::populate($data->content);
            Cache::store('redis')->put([$cacheName => $entity], now()->addHour());
        }

        return $entity;
    }

    /**
     * @param $name
     * @return null
     */
    public function __get($name)
    {
        $methodName = 'get' . ucfirst($name);
        if (method_exists(get_called_class(), $methodName)) {
            return $this->$methodName();
        } else if (isset($this->data->$name)) {
            return $this->data->$name;
        }

        return null;
    }

    /**
     * @param $name
     * @param $value
     */
    public function __set($name, $value)
    {
        $this->data->$name = $value;
    }

    /**
     * @param $data
     * @return array
     */
    protected static function populateMultiple($data)
    {
        $collection = [];

        if (empty($data) || !isset($data->content->results)) {
            return $collection;
        }

        foreach ($data->content->results as $entity) {
            $collection[] = static::populate($entity);
        }

        return $collection;
    }

    /**
     * @param $entity
     * @return null
     */
    protected static function populate($entity)
    {
        if (empty($entity) || (isset($entity->status) && $entity->status !== static::RESPONSE_STATUS_OK)) {
            return null;
        }
        $model = get_called_class();

        $object = new $model();
        $object->data = $entity;

        return $object;
    }

    /**
     * Returns offset value from paging url
     * @param string $url
     * @return null
     */
    protected static function findOffset(string $url)
    {
        $parts = parse_url($url);
        parse_str($parts['query'], $query);

        return $query['offset'] ?? null;
    }
}

