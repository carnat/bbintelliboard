<?php

namespace App\Api\v1;

class InstitutionRole extends ActiveResource
{
    /**
     * @inheritdoc
     */
    protected static function endpointName()
    {
        return 'institutionRoles';
    }
}
