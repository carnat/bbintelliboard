<?php

namespace App\Api\v1;

class User extends ActiveResource
{
    /**
     * @inheritdoc
     */
    protected static function endpointName()
    {
        return 'users';
    }

    /**
     * Returns all assigned courses
     * @param bool $withCache Enable or disable cache
     * @return array
     */
    public function courses(bool $withCache = true)
    {
        $endpoint = static::endpointName() .'/'. $this->id . '/courses';
        return Course::find([], $endpoint, $withCache);
    }
}
