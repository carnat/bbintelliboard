<?php

namespace App\Api\v1;

class Course extends ActiveResource
{
    /**
     * @inheritdoc
     */
    protected static function endpointName()
    {
        return 'courses';
    }

    /**
     * Returns one or all assigned users
     * @param null|string|null $id
     * @param bool $withCache Enable or disable cache
     * @return ActiveResource|array
     */
    public function users(?string $id = null, bool $withCache = true)
    {
        $endpoint = static::endpointName() .'/'. $this->id . '/users';
        return $id ? User::findOne($id, $endpoint, $withCache) :
            User::find([], $endpoint, $withCache);
    }

    /**
     * Returns one or all children for course
     * @param null|string|null $id
     * @param bool $withCache Enable or disable cache
     * @return ActiveResource|array
     */
    public function children(?string $id = null, bool $withCache = true)
    {
        $endpoint = static::endpointName() .'/'. $this->id . '/children';
        return $id ? Course::findOne($id, $endpoint, $withCache) :
            Course::find([], $endpoint, $withCache);
    }

    /**
     * Returns one or all contents
     * @param null|string|null $id
     * @param bool $withCache Enable or disable cache
     * @return ActiveResource|array
     */
    public function contents(?string $id = null, bool $withCache = true)
    {
        $endpoint = static::endpointName() .'/'. $this->id . '/contents';
        return $id ? Content::findOne($id, $endpoint, $withCache) :
            Content::find([], $endpoint, $withCache);
    }

    /**
     * Returns one or all course's groups
     * @param null|string|null $id
     * @param bool $withCache Enable or disable cache
     * @return ActiveResource|array
     */
    public function groups(?string $id = null, bool $withCache = true)
    {
        $endpoint = static::endpointName() .'/'. $this->id . '/groups';

        if ($id) {
            $result = CourseGroup::findOne($id, $endpoint, $withCache);
            $result->courseId = $this->id;
        } else {
            $result = CourseGroup::find([], $endpoint, $withCache);
            foreach ($result as $column) {
                $column->courseId = $this->id;
            }
        }
        return $result;
    }

    /**
     * Returns one or all course gradebook's periods
     * @param null|string|null $id
     * @param bool $withCache Enable or disable cache
     * @return ActiveResource|array
     */
    public function gradebookPeriods(?string $id = null, bool $withCache = true)
    {
        $endpoint = static::endpointName() .'/'. $this->id . '/gradebook/periods';
        return $id ? GradebookPeriod::findOne($id, $endpoint, $withCache) :
            GradebookPeriod::find([], $endpoint, $withCache);
    }

    /**
     * Returns one or all course gradebook's notations
     * @param null|string|null $id
     * @param bool $withCache Enable or disable cache
     * @return ActiveResource|array
     */
    public function gradebookNotations(?string $id = null, bool $withCache = true)
    {
        $endpoint = static::endpointName() .'/'. $this->id . '/gradebook/gradeNotations';
        return $id ? GradebookNotation::findOne($id, $endpoint, $withCache) :
            GradebookNotation::find([], $endpoint, $withCache);
    }

    /**
     * Returns one or all course gradebook's schemas
     * @param null|string|null $id
     * @param bool $withCache Enable or disable cache
     * @return ActiveResource|array
     */
    public function gradebookSchemas(?string $id = null, bool $withCache = true)
    {
        $endpoint = static::endpointName() .'/'. $this->id . '/gradebook/schemas';
        return $id ? GradebookSchema::findOne($id, $endpoint, $withCache) :
            GradebookSchema::find([], $endpoint, $withCache);
    }

    /**
     * @param string $id
     * @param bool $withCache Enable or disable cache
     * @return ActiveResource
     */
    public function gradebookUsers(string $id, bool $withCache = true) : ActiveResource
    {
        $endpoint = static::endpointName() .'/'. $this->id . '/gradebook/users';
        return GradebookSchema::findOne($id, $endpoint, $withCache);
    }

    /**
     * @param string $id Task ID
     * @param bool $withCache Enable or disable cache
     * @return ActiveResource
     */
    public function gradebookTasks(string $id, bool $withCache = true) : ActiveResource
    {
        $endpoint = static::endpointName() .'/'. $this->id . '/tasks';
        return CourseTask::findOne($id, $endpoint, $withCache);
    }

    /**
     * Returns one or all course gradebook's columns
     * @param null|string|null $id
     * @param bool $withCache Enable or disable cache
     * @return ActiveResource|array
     */
    public function gradebookColumns(?string $id = null, bool $withCache = true)
    {
        $endpoint = static::endpointName() .'/'. $this->id . '/gradebook/columns';

        if ($id) {
            $result = GradebookColumn::findOne($id, $endpoint, $withCache);
            $result->courseId = $this->id;
        } else {
            $result = GradebookColumn::find([], $endpoint, $withCache);
            foreach ($result as $column) {
                $column->courseId = $this->id;
            }
        }
        return $result;
    }
}
