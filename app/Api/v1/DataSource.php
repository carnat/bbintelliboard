<?php

namespace App\Api\v1;

class DataSource extends ActiveResource
{
    /**
     * @inheritdoc
     */
    protected static function endpointName()
    {
        return 'dataSources';
    }
}
