<?php

namespace App\Api\v1;

class CourseRole extends ActiveResource
{
    /**
     * @inheritdoc
     */
    protected static function endpointName()
    {
        return 'courseRoles';
    }
}
