<?php

namespace App\Api\v1;

class LtiPlacement extends ActiveResource
{
    /**
     * @inheritdoc
     */
    protected static function endpointName()
    {
        return 'lti/placements';
    }
}
