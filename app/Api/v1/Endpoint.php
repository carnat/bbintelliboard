<?php

namespace App\Api\v1;

use Ixudra\Curl\Facades\Curl;
use stdClass;

abstract class Endpoint
{
    const RESPONSE_STATUS_OK = 200;

    /**
     * @return string
     */
    protected static function endpointName()
    {
        return '';
    }

    /**
     * Api URL from config
     * @return mixed
     */
    protected static function getURL() : ?string
    {
        //TODO in future here must be client's instance BB URL (from database)
        return config('params.apiUrl');
    }

    /**
     * Send API GET request
     * @param array $params
     * @param string $endpoint
     * @return null|stdClass
     * @throws \Exception
     */
    protected static function get(array $params = [], $endpoint) : ?stdClass
    {
        $url = static::getURL() .'/'. ($endpoint ?? static::endpointName());

        if (isset($params['id'])) {
            $url .= '/' . $params['id'];
        } elseif (!empty($params)) {
            $url .= '?';
            foreach ($params as $param => $value) {
                $url .= $param.'='.$value.'&';
            }
        }

        $response = Curl::to($url)
            ->withHeader('Authorization : Bearer ' . static::getToken())
            ->returnResponseObject()
            ->get();

        $response->content = json_decode($response->content);

        return $response;
    }

    /**
     * Returns API access token
     * @return string
     * @throws \Exception
     */
    protected static function getToken() : string
    {
        if (!$token = cache('bbToken')) {
            $response = Curl::to(static::getURL() .'/oauth2/token')
                ->withData(['grant_type' => 'client_credentials'])
                //TODO client's API ket and Secret from database
                ->withHeader(
                    'Authorization : Basic '.base64_encode(config('params.apiKey').':'.config('params.apiSecret'))
                )
                ->returnResponseObject()
                ->post();

            $data = json_decode($response->content);
            $token = $data->access_token ?? null;
            if ($response->status == static::RESPONSE_STATUS_OK && $token) {
                cache(['bbToken' => $token], now()->addSeconds($data->expires_in));
            } else {
                throw new \Exception('Error while receive API access token');
            }
        }

        return $token;
    }
}
