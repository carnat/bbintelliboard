<?php

namespace App\Api\v1;

class LtiDomain extends ActiveResource
{
    /**
     * @inheritdoc
     */
    protected static function endpointName()
    {
        return 'lti/domains';
    }
}
