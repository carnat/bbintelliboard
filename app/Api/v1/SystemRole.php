<?php

namespace App\Api\v1;

class SystemRole extends ActiveResource
{
    /**
     * @inheritdoc
     */
    protected static function endpointName()
    {
        return 'systemRoles';
    }
}
