<?php

namespace App\Api\v1;

class Term extends ActiveResource
{
    /**
     * @inheritdoc
     */
    protected static function endpointName()
    {
        return 'terms';
    }
}
