<?php

namespace App\Api\v1;

class System extends ActiveResource
{
    /**
     * @inheritdoc
     */
    protected static function endpointName()
    {
        return 'system';
    }
}
