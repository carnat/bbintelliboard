<?php

namespace App\Api\v1;

class GradebookColumn extends ActiveResource
{
    /**
     * @inheritdoc
     */
    protected static function endpointName()
    {
        return '';
    }

    /**
     * Returns one or all attempts for course gradebook's column
     * @param null|string|null $id
     * @param bool $withCache Enable or disable cache
     * @return ActiveResource|array
     */
    public function attempts(?string $id = null, bool $withCache = true)
    {
        $endpoint = 'courses/'. $this->courseId . '/gradebook/columns/'. $this->id . '/attempts';
        return $id ? ColumnAttempt::findOne($id, $endpoint, $withCache) :
            ColumnAttempt::find([], $endpoint, $withCache);
    }

    /**
     * Returns one or all users for course gradebook's column
     * @param null|string|null $id
     * @param bool $withCache Enable or disable cache
     * @return ActiveResource|array
     */
    public function users(?string $id = null, bool $withCache = true)
    {
        $endpoint = 'courses/'. $this->courseId . '/gradebook/columns/' . $this->id . '/users';
        return $id ? ColumnUser::findOne($id, $endpoint, $withCache) :
            ColumnUser::find([], $endpoint, $withCache);
    }
}
