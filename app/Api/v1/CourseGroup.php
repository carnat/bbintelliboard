<?php

namespace App\Api\v1;

class CourseGroup extends ActiveResource
{
    /**
     * @inheritdoc
     */
    protected static function endpointName()
    {
        return '';
    }

    /**
     * Returns one or all users for course's group
     * @param null|string|null $id
     * @param bool $withCache Enable or disable cache
     * @return ActiveResource|array
     */
    public function users(?string $id = null, bool $withCache = true)
    {
        $endpoint = 'courses/'. $this->courseId . '/groups/'. $this->id . '/users';
        return $id ? CourseGroupUser::findOne($id, $endpoint, $withCache) :
            CourseGroupUser::find([], $endpoint, $withCache);
    }
}
