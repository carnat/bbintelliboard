<?php

namespace App\Http\Controllers;

use App\Api\v1\Course;
use App\Api\v1\LtiDomain;
use App\Api\v1\LtiPlacement;
use App\Api\v1\Term;
use App\Api\v1\User;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Auth;
use DB;
use Illuminate\Support\Facades\Redis;
use Log;

class DashboardController extends Controller
{
    public function index()
    {
//        $result = DB::connection('pgsql2')->table('bb_col_access_course')->get();
//dd($result);
        $gradebooks = DB::connection('pgsql2')->table('gradebook_log')->get();
        $users = DB::table('users')->get();
        $courses = DB::table('course_main')->where('course_id', '<>', 'SYSTEM')->get();
        $courseTools = DB::table('ods_course_tool')->get();
        $courseUsers = DB::table('course_users')->get();
        $sessionActivity = DB::table('ods_aa_session_activity')->get();
        $activityCompletions = DB::connection('pgsql2')->table('gradebook_log')->get();

        $currentOnline = 0;
        $currentLearners = 0;
        $currentTeachers = 0;
//        $uniqUsers = [];
//        foreach ($currentSessions as $onlineSession) {
//            foreach ($courseUsers as $courseUser) {
//                if (!in_array($courseUser->users_pk1, $uniqUsers) && $courseUser->users_pk1 == $onlineSession->user_id_pk1) {
//                    if ($courseUser->role == 'S') {
//                        $currentLearners++;
//                    } elseif ($courseUser->role == 'P') {
//                        $currentTeachers++;
//                    }
//                    $uniqUsers[] = $courseUser->users_pk1;
//                }
//
//            }
//        }

        $sumGrade = 0;
        foreach ($gradebooks as $gradebook) {
            $sumGrade += (int)$gradebook->grade;
            foreach ($users as $userGrade) {
                $userGrade->sumGrade = $userGrade->sumGrade ?? 0;
                if ($userGrade->pk1 == $gradebook->user_pk1) {
                    $userGrade->sumGrade += (int)$gradebook->grade;
                }
            }
        }
        $avgGrade = (int) ($sumGrade / count($gradebooks));

        //enrollments for chartjs
        $enrollmentsPerDay = [];
        foreach ($courseUsers as $courseUser) {
            if ($courseUser->role == 'S') {
                $date = new \DateTime($courseUser->enrollment_date);
                $enrollmentsPerDay[$date->format('Y-m-d')] = isset($enrollmentsPerDay[$date->format('Y-m-d')]) ?
                    $enrollmentsPerDay[$date->format('Y-m-d')] + 1 : 1;
            }

            foreach ($users as $user) {
                if ($user->pk1 == $courseUser->users_pk1) {
                    $user->enrollments = isset($user->enrollments) ?
                        $user->enrollments + 1 : 1;
                }
            }
        }

        $now = (new \DateTime())->sub(new \DateInterval('P1D'));
        $newUsers = 0;
        foreach ($users as $user) {
            $date = new \DateTime($user->dtcreated);
            if ($date->format('Y-m-d') == $now->format('Y-m-d')) {
                $newUsers++;
            }
        }
        //sessions for chartjs
        $sessionsPerDay = [];
        $timeSpent = 0;
        $summaryTimeSpent = 0;
        foreach ($sessionActivity as $session) {
            $sessionDate = new \DateTime($session->login_time);
            if (isset($sessionsPerDay[$sessionDate->format('Y-m-d')]) &&
                in_array($session->user_pk1, $sessionsPerDay[$sessionDate->format('Y-m-d')])) {
                continue;
            }
            $sessionsPerDay[$sessionDate->format('Y-m-d')][] = $session->user_pk1;
            $loginTime = new \DateTime($session->login_time);
            $logoutTime = new \DateTime($session->logout_time);
            $timeSpentInSession = $logoutTime->diff($loginTime)->format('%h') * 60 + $logoutTime->diff($loginTime)->format('%i');

            if ($loginTime->format('Y-m-d') == $now->format('Y-m-d')) {
                $timeSpent += $timeSpentInSession;
            }
            $summaryTimeSpent += $timeSpentInSession;

            foreach ($users as $timeForUser) {
                $timeForUser->timeSpent = $timeForUser->timeSpent ?? 0;
                if ($timeForUser->pk1 == $session->user_pk1) {
                    $timeForUser->timeSpent += $timeSpentInSession;
                }
            }
        }
        $avgTimeSpent = (int)($summaryTimeSpent / count($users));

        foreach ($sessionsPerDay as $key => $sessionUsers) {
            $sessionsPerDay[$key] = count($sessionUsers);
        }

        //activity completions for chartjs
        $completionPerDay = [];
        foreach ($activityCompletions as $completion) {
            $completeDate = new \DateTime($completion->date_logged);
            $completionPerDay[$completeDate->format('Y-m-d')] = isset($completionPerDay[$completeDate->format('Y-m-d')]) ?
                $completionPerDay[$completeDate->format('Y-m-d')] + 1 : 1;
        }

        $currentDate = new \DateTime();
        $dateLabels = [];
        for ($i = 0; $i < 9; $i++) {
            array_unshift($dateLabels, $currentDate->format('d. M'));
            $enrollmentsPerDay[$currentDate->format('Y-m-d')] = isset($enrollmentsPerDay[$currentDate->format('Y-m-d')]) ?
                $enrollmentsPerDay[$currentDate->format('Y-m-d')] : 0;
            $sessionsPerDay[$currentDate->format('Y-m-d')] = isset($sessionsPerDay[$currentDate->format('Y-m-d')]) ?
                $sessionsPerDay[$currentDate->format('Y-m-d')] : 0;
            $completionPerDay[$currentDate->format('Y-m-d')] = isset($completionPerDay[$currentDate->format('Y-m-d')]) ?
                $completionPerDay[$currentDate->format('Y-m-d')] : 0;
            $currentDate->sub(new \DateInterval('P1D'));
        }

        krsort($enrollmentsPerDay);
        krsort($completionPerDay);
        krsort($sessionsPerDay);
        $enrollmentsPerDay = array_slice($enrollmentsPerDay, 0, 9);
        $completionPerDay = array_slice($completionPerDay, 0, 9);
        $sessionsPerDay = array_slice($sessionsPerDay, 0, 9);
        ksort($enrollmentsPerDay);
        ksort($completionPerDay);
        ksort($sessionsPerDay);

        $weeks = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'];
        //page views
        $trackings = DB::table('system_tracking')->orderBy('timestamp', 'desc')->get();
        $views = $trackings[0]->page_views;

        $sumPages = 0;
        foreach ($trackings as $tracking) {
            $sumPages += $tracking->page_views;
        }
        $avgPages = (int) ($sumPages / count($users));

        return view('dashboard', compact(
            'users',
            'courses',
            'courseTools',
            'sessionActivity',
            'enrollmentsPerDay',
            'sessionsPerDay',
            'completionPerDay',
            'weeks',
            'now',
            'currentOnline',
            'currentLearners',
            'currentTeachers',
            'newUsers',
            'views',
            'timeSpent',
            'avgTimeSpent',
            'avgGrade',
            'avgPages'
        ));
    }

    public function getForChart()
    {
        $courseUsers = DB::table('course_users')->where('role', 'S')->get();
        $sessionActivity = DB::table('ods_aa_session_activity')->get();
        $activityCompletions = DB::connection('pgsql2')->table('gradebook_log')->get();
        //enrollments for chartjs
        $enrollmentsPerDay = [];
        foreach ($courseUsers as $user) {
            $date = new \DateTime($user->enrollment_date);
            $enrollmentsPerDay[$date->format('Y-m-d')] = isset($enrollmentsPerDay[$date->format('Y-m-d')]) ?
                $enrollmentsPerDay[$date->format('Y-m-d')] + 1 : 1;
        }

        //sessions for chartjs
        $sessionsPerDay = [];
        foreach ($sessionActivity as $session) {
            $sessionDate = new \DateTime($session->login_time);
            if (isset($sessionsPerDay[$sessionDate->format('Y-m-d')]) &&
                in_array($session->user_pk1, $sessionsPerDay[$sessionDate->format('Y-m-d')])) {
                continue;
            }
            $sessionsPerDay[$sessionDate->format('Y-m-d')][] = $session->user_pk1;
        }

        foreach ($sessionsPerDay as $key => $users) {
            $sessionsPerDay[$key] = count($users);
        }

        //activity completions for chartjs
        $completionPerDay = [];
        foreach ($activityCompletions as $completion) {
            $completeDate = new \DateTime($completion->date_logged);
            $completionPerDay[$completeDate->format('Y-m-d')] = isset($completionPerDay[$completeDate->format('Y-m-d')]) ?
                $completionPerDay[$completeDate->format('Y-m-d')] + 1 : 1;
        }

        $currentDate = new \DateTime();
        $dateLabels = [];
        for ($i = 0; $i < 9; $i++) {
            array_unshift($dateLabels, $currentDate->format('d. M'));
            $enrollmentsPerDay[$currentDate->format('Y-m-d')] = isset($enrollmentsPerDay[$currentDate->format('Y-m-d')]) ?
                $enrollmentsPerDay[$currentDate->format('Y-m-d')] : 0;
            $sessionsPerDay[$currentDate->format('Y-m-d')] = isset($sessionsPerDay[$currentDate->format('Y-m-d')]) ?
                $sessionsPerDay[$currentDate->format('Y-m-d')] : 0;
            $completionPerDay[$currentDate->format('Y-m-d')] = isset($completionPerDay[$currentDate->format('Y-m-d')]) ?
                $completionPerDay[$currentDate->format('Y-m-d')] : 0;
            $currentDate->sub(new \DateInterval('P1D'));
        }

        krsort($enrollmentsPerDay);
        krsort($completionPerDay);
        krsort($sessionsPerDay);
        $enrollmentsPerDay = array_slice($enrollmentsPerDay, 0, 9);
        $completionPerDay = array_slice($completionPerDay, 0, 9);
        $sessionsPerDay = array_slice($sessionsPerDay, 0, 9);
        ksort($enrollmentsPerDay);
        ksort($completionPerDay);
        ksort($sessionsPerDay);

        return new JsonResponse([
            'enrollmentsPerDay'=> array_values($enrollmentsPerDay),
            'sessionsPerDay' => array_values($sessionsPerDay),
            'completionPerDay' => array_values($completionPerDay),
            'dateLabels' => $dateLabels
        ]);
    }

    public function caliper(Request $request)
    {
        Log::info('dumpRequest', ['request' => $request->post()]);
    }

    public function test()
    {
//        Redis::set('name', 'Taylor');
//        $values = Redis::lrange('names', 5, 10);
//        dd($values);
        /* @var User $course*/
        $course = Course::find();
        dd($course);
    }
}
