/**
 * Created by carnat on 14.03.18.
 */
$(document).ready(function () {
    let chartColors = {
        red: 'rgb(255, 99, 132)',
        orange: 'rgb(255, 159, 64)',
        yellow: 'rgb(255, 205, 86)',
        green: 'rgb(0, 255, 0)',
        blue: 'rgb(0, 0, 255)',
        purple: 'rgb(153, 102, 255)',
        grey: 'rgb(201, 203, 207)',
        black: 'rgb(0, 0, 0)'
    };

    let color = Chart.helpers.color;
    var ctx = document.getElementById('canvas').getContext('2d');
    $.get('/get-for-chart', function(data) {
        let myBar = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: data.dateLabels,
                datasets: [{
                    type: 'line',
                    label: 'Number of sessions',
                    fill: false,
                    borderColor: chartColors.blue,
                    data: data.sessionsPerDay,
                }, {
                    type: 'line',
                    label: 'Activity completions',
                    fill: false,
                    borderColor: chartColors.black,
                    data: data.completionPerDay,
                }, {
                    type: 'line',
                    label: 'User Enrollments',
                    fill: false,
                    borderColor: chartColors.green,
                    data: data.enrollmentsPerDay,
                }]
            },
            options: {
                responsive: true,
                maintainAspectRatio: false,
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true,
                        }
                    }]
                },
                elements: {
                    line: {
                        tension: 0, // disables bezier curves
                    }
                }
            }
        });
    });
});